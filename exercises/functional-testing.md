# Functional testing

Functional testing is validating if the software is doing what is suppose to do from a functional perspective.

## Goals

![Automated test pyramid](images/automated-testing-pyramid.png)

The test-pyramid is a great strategy for testing. On the lowest section of this pyramid unit tests are defined and 
these tests should be very fast and should cover about 90% of the application. The middle layer defines integration 
between technical component and about 70% of the application should be covered. The top part of the pyramid is 
end-to-end testing that should cover about 20% of the application.

The manual testing part should only be focused on the following:

- Does it look right
- Does it feel right
- Focus on change

These things are hard to understand for a computer.

## Approach

For unit testing and integration testing there are different approaches. In this exercise, unit testing is done with the 
[Jest framework](https://jestjs.io/) and integration testing is done using a docker container that has a AWS lambda 
architecture.

## Exercise

Our task now is to create a test stage with two functional testing jobs. The first job is the unit test job that is run 
with jest. The second job is AWS docker container that simulates a AWS lambda integration.

### Step 1: Unit testing

Create a unitstep and a test stage in the continuous delivery pipeline. Add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
stages:
# Disable
#  - sample
  - qa
  - build
  - test

# The unit test job
unittest:
  stage: test
  script:
  - npm test
```

The stage **test** is an new logical divider of steps within the continuous delivery pipeline. The job **unittest** is 
actual command that starts the unit testing of the application. 

### Step 2: Integration testing

Create a integration test job in the continuous delivery pipeline. Add the following parts to 
the [.gitlab-ci.yml](../.gitlab-ci.yml) 

```yaml
# The integration job
integration_test:
  stage: test
  before_script: []
  image: docker:dind
  services:
  - name: docker:dind
    alias: docker
  script:
  - "docker run --rm -v \"$PWD/src\":/var/task lambci/lambda:nodejs8.10 index.handler '{\"side1\" : 1, \"side2\" : 2, \"side3\" : 4}'"

```

A new job integration_test is added to the test stage

